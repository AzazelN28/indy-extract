#include <string.h>
#include <stdlib.h>
#include <stdio.h>

typedef struct {
    unsigned char r;
    unsigned char g;
    unsigned char b;
} rgb_t;

typedef struct {
    unsigned char r;
    unsigned char g;
    unsigned char b;
    unsigned char a;
} rgba_t;

int main(int argc, char** argv) {

    if (argc < 3) {
        printf("Use: pal2act <in> <out>\n");
        return -1;
    }

    int reverse = 0;

    if (argc == 4) {
        if (strcmp(argv[3],"-i") == 0) {
            reverse = 1;
        }
    }

    FILE* in;
    FILE* out;

    in = fopen(argv[1], "r");
    if (in == NULL) {
        printf("Can't open %s\n", argv[1]);
        return -1;
    }
    
    out = fopen(argv[2], "w");
    if (out == NULL) {
        printf("Can't open %s\n", argv[2]);
        return -1;
    }

    rgba_t icolors[256];
    fread(&icolors, sizeof(icolors), 1, in);

    rgb_t ocolors[256];

    int i = 0;
    for (i = 0; i < 256; i++) {
        if (reverse == 1) {
            ocolors[i].r = icolors[255 - i].r;
            ocolors[i].g = icolors[255 - i].g;
            ocolors[i].b = icolors[255 - i].b;
        } else {
            ocolors[i].r = icolors[i].r;
            ocolors[i].g = icolors[i].g;
            ocolors[i].b = icolors[i].b;
        }
    }

    fwrite(&ocolors, sizeof(ocolors), 1, out);

    return 0;

}
